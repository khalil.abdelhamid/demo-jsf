FROM tomcat:9-jdk8
ADD target/demo.war /usr/local/tomcat/webapps/
EXPOSE 1337
CMD ["catalina.sh", "run"]
