# Demo JSF / JSP Project


# Installation

```
# This will create .war file
> mvn package

# Build docker images
> docker build -t demo:app .

# Run the container
> docker run --rm -p 1337:8080 demo:app
```

Then open on your browser `http://localhost:1337/demo`
