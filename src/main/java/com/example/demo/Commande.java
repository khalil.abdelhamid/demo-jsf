package com.example.demo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.Date;

@ManagedBean(name = "commande", eager = true)
@SessionScoped
@Getter
@Setter
@NoArgsConstructor
public class Commande {

    private Date date = new Date();
    private float montant;
    private String modePaiement;
    private String statusPaiement;
    private String modeLivraison;
    private String statusLivraison;
}
