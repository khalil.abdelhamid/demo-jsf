package com.example.demo;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class Helloworld {

    public Helloworld() {
        System.out.println("Helloworld Bean instantiated");
    }

    public String getMessage() {
        return "Helloworld beans says HI !!!";
    }
}
